import React from "react";
import BibliotecaScreen from "../containers/Biblioteca";

const biblioteca = () => <BibliotecaScreen />;

export default biblioteca;
