import React from "react";
// import { Head } from "next/document";
import Global from "../components/Global";

export default ({ Component, pageProps }) => {
  return (
    <>
      <Global />
      {/* <Head>
      <title>IESTA: Instituto Estadual Santo Tomás de Aquino</title>
    </Head> */}
      <Component {...pageProps} />
    </>
  );
};
