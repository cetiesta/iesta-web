import React, { Fragment } from "react";
import Document, { Html, Head, Main, NextScript } from "next/document";
import { ServerStyleSheet } from "styled-components";

class Documenta extends Document {
  static async getInitialProps(context) {
    const sheet = new ServerStyleSheet();
    const originalRenderPage = context.renderPage;

    try {
      context.renderPage = () =>
        originalRenderPage({
          enhanceApp: App => props => sheet.collectStyles(<App {...props} />)
        });

      const initialProps = await Document.getInitialProps(context);
      return {
        ...initialProps,
        styles: (
          <Fragment>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </Fragment>
        )
      };
    } finally {
      sheet.seal();
    }
  }

  render() {
    return (
      <>
        <Html lang="pt-BR">
          <Head>
            <link
              rel="stylesheet"
              type="text/css"
              href="fonts.googleapis.com/css?family=Roboto|Open+Sans&display=swap"
            />
            <title>IESTA: Instituto Estadual Santo Tomás de Aquino</title>
          </Head>
          <body>
            <div className="root"></div>
            <Main />
            <NextScript />
          </body>
        </Html>
      </>
    );
  }
}

export default Documenta;
