const express = require("express");
const next = require("next");
const database = require("./server");
const config = require("./server/config.json");
const router = require("./server/router");
const axios = require("axios");

const development = process.env.NODE_ENV !== "production";
const application = next({ development });
const handle = application.getRequestHandler();

application.prepare().then(() => {
  const server = express();

  server.use("/api", router);

  server.get("*", (request, response) => {
    console.log(`[IESTA] Requisição em: ${request.hostname}`);

    axios
      .get(`https://ipinfo.io/json?ip=${request.hostname}`)
      .then(result => {
        // console.log("IESTA> IpInfo recebido:", result.data);
        const { data } = result;
        console.log("[IESTA] Uma requisição vinda da cidade de", data.city);
        console.log("[IESTA] Uma requisição de IP", data.ip);
      })
      .catch(error => error);

    handle(request, response);
  });

  server.listen(config.port, error => {
    if (error) {
      throw error;
    }
    database();
    console.log(`[IESTA] Preparado em https://${config.host}:${config.port}`);
  });
});
