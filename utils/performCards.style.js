import styled from "styled-components";
import colorPalette from "./colorPalette";

export const CardContainer = styled.div`
  margin: 18px;
`;

export const CardTitle = styled.h2`
  margin-top: 4px;
  font-family: "Roboto", "Open Sans", sans-serif;
  font-weight: bold;
  text-align: center;
`;

export const CardDescription = styled.p`
  font-family: "Roboto", sans-serif;
  color: ${colorPalette.white};
  font-size: 16px;
  margin: 12px;
`;
