import React from "react";
import PropsTypes, { string, node } from "prop-types";

import { Grid, Paper, Typography } from "@material-ui/core";
import * as styled from "./performCards.style";
import colorPalette from "./colorPalette";

const performCards = content => {
  if (content) {
    return (
      <styled.CardContainer>
        <Grid container spacing={1}>
          {content.map(data => {
            return (
              <Grid item xs={12} md={4} lg={4}>
                <Paper
                  variant="outlined"
                  square
                  style={{
                    minHeight: "100%",
                    borderRadius: 6,
                    backgroundColor: colorPalette.primary.medium
                  }}
                >
                  <Typography
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      color: colorPalette.white
                    }}
                  >
                    {data.icon}
                    <strong>{data.title}</strong>
                  </Typography>
                  <Typography
                    align="justify"
                    style={{
                      margin: 8
                    }}
                  >
                    <styled.CardDescription>
                      {data.description}
                    </styled.CardDescription>
                  </Typography>
                </Paper>
              </Grid>
            );
          })}
        </Grid>
      </styled.CardContainer>
    );
  }
};

performCards.propTypes = {
  content: PropsTypes.arrayOf(
    PropsTypes.shape({
      title: string,
      description: PropsTypes.oneOf([string, node]),
      icon: node
    })
  )
};

export default performCards;
