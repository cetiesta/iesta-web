module.exports = {
  white: "#fff",
  black: "#000",
  theme: "#000050",
  primary: {
    light: "#00afff",
    medium: "#000090",
    dark: "#000050"
  },
  secundary: {
    light: "#fff",
    medium: "#eee",
    dark: "#333"
  }
};
