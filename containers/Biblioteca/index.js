import React, { useState } from "react";
import { Footer, Global, Header } from "../../components";

// Páginas
import SignInPage from "./signin";

const pages = [{ key: "signin", component: SignInPage }];

export default function Biblioteca(props) {
  const [currentPage, switchPage] = useState(pages[0]);
  return (
    <>
      <Global />
      <Header />
      <> {React.createElement(currentPage.component, { ...props })} </>
      <Footer />
    </>
  );
}
