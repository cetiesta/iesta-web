import React from "react";
import styled from "styled-components";
import { Paper, Grid, Button, TextField, Tooltip } from "@material-ui/core";
import { Field, Form } from "react-final-form";

const Container = styled.div`
  font-family: "Roboto", "Open Sans", sans-serif;
  font-size: 16px;
  background-image: linear-gradient(to left, #6a11cb 0%, #2575fc 100%);
`;

const LoginPaper = styled(Paper)`
  display: flex;
  border-radius: 4px;
  justify-content: center;
  height: 12vh;
  width: 100%;
  margin: 12px;
`;

export default function SignInPage(props) {
  return (
    <Container>
      <Form
        onSubmit={() => {}}
        initialValues={{}}
        render={(formProps) => (
          <Grid container xs={12} md={12} lg={12}>
            <Grid item xs={12} md={12} lg={12}>
              <LoginPaper elevation={0}>
                <Grid container xs={12} spacing={3}>
                  <Grid item xs={12} md={12} lg={12}>
                    <Field
                      {...props}
                      {...formProps}
                      maxLength={16}
                      name="token_acesso"
                      style={{ marginBottom: 8 }}
                    >
                      {() => (
                        <Tooltip title="O Token de Acesso é composto por 8 caracteres.">
                          <TextField
                            id="token_acesso"
                            label="Token de Acesso"
                            variant="outlined"
                          />
                        </Tooltip>
                      )}
                    </Field>
                  </Grid>
                  <Grid item xs={12} md={12} lg={12}>
                    <Button type="submit" variant="outlined" color="primary">
                      Entrar
                    </Button>
                  </Grid>
                </Grid>
              </LoginPaper>
            </Grid>
          </Grid>
        )}
      />
    </Container>
  );
}
