import styled from "styled-components";
import { Robot } from "styled-icons/fa-solid/Robot";
// import { CircuitBoard } from "styled-icons/octicons/CircuitBoard";
// import { Arduino } from "./icons/Arduino";

// export const ArduinoIcon = styled(Arduino)`
//   color: #000;
//   height: 24px;
//   width: 24px;
// `;

export const CleitonIcon = styled(Robot)`
  width: 42px;
  height: 42px;
  color: #000050;
  border: 0 solid #fff;
  border-radius: 4px;
  margin: 12px;
`;
