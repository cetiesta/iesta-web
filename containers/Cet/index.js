import React from "react";
import Header from "../../components/Header";
import Global from "../../components/Global";
import Footer from "../../components/Footer";
import * as styled from "./style";
// import * as icons from './icons';
import Typist from "react-typist";
import performCards from "../../utils/performCards";
import BugReportIcon from "@material-ui/icons/BugReport"; // <-- bug_report
import EmojiFlagsIcon from "@material-ui/icons/EmojiFlags"; // <-- emoji_flags
import CodeIcon from "@material-ui/icons/Code"; // <-- code
import { Typography, Paper } from "@material-ui/core";
import colorPalette from "../../utils/colorPalette";

const messages = [
  {
    title: "Robótica",
    description: (
      <span>
        O Núcleo de Ciência e Tecnologia do Instituto Estadual Santo Tomás de
        Aquino faz parte da OBR, (
        <strong>
          <a
            href="http://www.obr.org.br/"
            style={{
              textDecoration: "none",
              color: colorPalette.primary.light,
            }}
            target="_blank"
          >
            Olimpíada Brasileira de Robótica
          </a>
        </strong>
        ), e possui diversos prêmios por isso. É uma cultura dentro do instituto
        que os alunos do Terceiro Ano do Ensino Médio participem do lançamento
        de foguetes em nome do IESTA.
      </span>
    ),
    icon: <BugReportIcon />,
  },
  {
    title: "Campeonatos",
    description: (
      <span>
        O IESTA é reconhecido com diversos prêmios acumulados ao decorrer dos
        anos, isso pois nossa filosofia é <strong>aprender para ser</strong>,{" "}
        <strong>para saber</strong> e <strong>para fazer</strong>.
      </span>
    ),
    icon: <EmojiFlagsIcon />,
  },
  {
    title: "Desenvolvimento",
    description: (
      <span>
        Nós utilizados de ferramentas para que os alunos aprendam, de forma
        interativa, o desenvolvimento de sistemas, desde de seu hardware (baixo
        nível), como o{" "}
        <strong>
          <a
            style={{
              textDecoration: "none",
              color: colorPalette.primary.light,
            }}
            href="https://arduino.gg"
            target="_blank"
          >
            Arduino
          </a>
        </strong>
        .
      </span>
    ),
    icon: <CodeIcon />,
  },
];

export default function CetScreen() {
  return (
    <>
      <Global />
      <Header />
      <styled.Container>
        <styled.Area>
          <div>
            <styled.TerminalLook>
              <p style={{ position: "relative", top: 0, left: 0 }}>
                <b>
                  {" "}
                  $ Terminal{" "}
                  <span style={{ color: "#0496ff" }}>
                    &rarr; Núcleo de Ciência e Tecnologia
                  </span>
                </b>
              </p>
              <Typist
                cursor={{
                  show: true,
                  blink: true,
                  element: "",
                  hideWhenDone: true,
                  hideWhenDoneDelay: 0,
                }}
              >
                <styled.Message>
                  <code style={{ display: "flex" }}>
                    <div style={{ color: "#0000f0" }}> >> </div>
                    <div style={{ color: "#0496FF" }}>print('</div>
                    <div style={{ color: "#fff" }}>
                      Todo mundo devia aprender a ensinar...
                    </div>
                    <div style={{ color: "#0496FF" }}>');</div>

                    <Typist.Backspace delay={750} count={13} />
                    <div style={{ color: "#fff" }}>pensar...</div>
                    <div style={{ color: "#0496FF" }}>');</div>

                    <Typist.Backspace delay={750} count={12} />
                    <div style={{ color: "#fff" }}>programar...</div>
                    <div style={{ color: "#0496FF" }}>');</div>
                  </code>
                  <Typist.Delay ms={500} />
                  <code style={{ display: "flex" }}>
                    <br />
                    <div style={{ color: "#0000f0" }}> >> </div>
                    <div style={{ color: "#0496FF" }}>print('</div>
                    <div style={{ color: "#fff" }}>
                      Pois programar te ensina a pensar.
                    </div>
                    <div style={{ color: "#0496FF" }}>');</div>
                  </code>
                </styled.Message>
                <Typist.Delay ms={1000} /> <br />
                <span style={{ display: "flex", justifyContent: "center" }}>
                  <b>
                    <i>~~ Steve Jobs</i>
                  </b>
                </span>
              </Typist>
            </styled.TerminalLook>
            <br />
          </div>
        </styled.Area>
        {performCards(messages)}
      </styled.Container>
      <styled.ProgressiveContainer></styled.ProgressiveContainer>
      <Footer />
    </>
  );
}
