import styled from "styled-components";

export const Container = styled.div`
  background-image: linear-gradient(to bottom, #4facfe 0%, #00f2fe 100%);
  color: #000;
  margin: 0 auto;
  min-height: 50vh;
  height: 100%;
  white-space: wrap;
`;

export const ProgressiveContainer = styled.div`
  background-image: linear-gradient(to bottom, #00f2fe 0%, #fff 100%);
  color: #000;
  margin: 0 auto;
  min-height: 50vh;
  height: 100%;
  white-space: wrap;
`;

export const CleitonArea = styled.div`
  color: #000060;
  font-family: "Roboto", "Open Sans", Arial, Helvetica, sans-serif;
  font-weight: normal;
  background-image: linear-gradient(
    to bottom,
    #f5f5f5 0%,
    #ffffff 100%
  ) !important;
`;

export const CleitonInnerArea = styled.div`
  margin: 60px;
  display: flex;
  justify-content: space-evenly;
  border: 0 solid #fff;
  border-radius: 8px;
  background: #fff;
`;

export const TerminalLook = styled.div`
  background: #000;
  border: 0 solid #000;
  border-radius: 12px;
  width: 60vw;
  min-height: 30vh;
  color: #fff;
  padding: 24px;
  white-space: pre-wrap;
  margin: 42px;

  &:hover {
    cursor: default;
  }
`;

export const Message = styled.h3`
  font-size: 18px;
  font-family: "Fira Code", "Courier New", "Courier", monospace;
  font-weight: bold;
`;

export const Area = styled.div`
  display: flex;
  justify-content: center;
`;
