import React, { useState } from "react";
import Header from "../../components/Header";
import {
  Background,
  Content,
  Container,
  GoodMessage,
  IconCentre,
  GoodPhrase,
  IconHub,
  IconBase,
  ToFacebookLink,
  MapContainer,
} from "./style";
import Global from "../../components/Global";
import {
  CetIcon,
  GestaIcon,
  InovacaoIcon,
  GitIcon,
  MapIcon,
  HistoryIcon,
  SocialIcon,
  CriatividadeIcon,
} from "./icons";
import Footer from "../../components/Footer";

export default function HomeScreen() {
  const [header, replaceHeader] = useState({
    title: "Ciência e Tecnologia",
    description: "Participe do Núcleo CeT e descubra o poder da tecnologia",
  });
  return (
    <>
      <Header />
      <Global />
      <Background>
        <Content>
          <Container>
            <GoodMessage>
              <ul>
                <li style={{ listStyle: "none" }}>
                  <GoodPhrase>
                    <b>
                      <u>CONECTAR</u>
                    </b>{" "}
                    CONHECIMENTOS.
                  </GoodPhrase>
                </li>
                <li style={{ listStyle: "none" }}>
                  <GoodPhrase>
                    <b>
                      <u>VIVENCIAR</u>
                    </b>{" "}
                    NOVAS EXPERIÊNCIAS.
                  </GoodPhrase>
                </li>
                <li style={{ listStyle: "none" }}>
                  <GoodPhrase>
                    <b>
                      <u>DESENVOLVER</u>
                    </b>{" "}
                    PARA A COMUNIDADE.
                  </GoodPhrase>
                </li>
                <li style={{ listStyle: "none" }}>
                  <GoodPhrase>
                    <b>
                      <u>INOVAR</u>
                    </b>{" "}
                    PARA O MUNDO.
                  </GoodPhrase>
                </li>
              </ul>
            </GoodMessage>
            <div></div>
          </Container>
        </Content>
      </Background>
      <div style={{ height: `80vh`, alignItems: "center" }}>
        <Container style={{ justifyContent: "center", marginTop: `30%` }}>
          <h1>O Poder da Educação</h1>
        </Container>
        <Container>
          <div></div>
          <div>
            <IconCentre>
              <div>
                <a href="/cet">
                  <CetIcon
                    onMouseOver={() => {
                      replaceHeader({
                        title: "Ciência e Tecnologia",
                        description:
                          "Participe do Núcleo CeT e descubra o poder da tecnologia",
                      });
                    }}
                  />
                </a>
              </div>
              <div>
                <a href="/gesta">
                  <GestaIcon
                    onMouseOver={() => {
                      replaceHeader({
                        title: "Grêmio Estudantil",
                        description:
                          "A diretoria está sempre junto aos alunos, representados pelo GESTA.",
                      });
                    }}
                  />
                </a>
              </div>
              <div>
                <a
                  target="__blank"
                  href="http://portal.educacao.rs.gov.br/novo-ensino-medio"
                >
                  <InovacaoIcon
                    onMouseOver={() => {
                      replaceHeader({
                        title: "Inovação",
                        description:
                          "O Instituto Estadual Santo Tomás de Aquino é uma das escolas piloto do Novo Ensino Médio.",
                      });
                    }}
                  />
                </a>
              </div>
              <div>
                <a target="__blank" href="https://gitlab.com/cetiesta">
                  <GitIcon
                    onMouseOver={() => {
                      replaceHeader({
                        title: "Nossos Projetos",
                        description:
                          "Seja nossos repositórios onde os projetos do CeT estão!",
                      });
                    }}
                  />
                </a>
              </div>
            </IconCentre>
          </div>
          <div></div>
        </Container>
        <Container>
          <div></div>
          <div>
            <h2 style={{ textAlign: "center" }}>{header.title}</h2>
            <h4>{header.description}</h4>
          </div>
          <div></div>
        </Container>
      </div>
      <MapContainer>
        <Container>
          <div></div>
          <div>
            <div style={{ display: "flex", justifyContent: "center" }}>
              <MapIcon />
              <h2 style={{ color: "#000050" }}>Nos visite</h2>
            </div>
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14031.422281251835!2d-52.1998487!3d-28.4537699!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7158ed11ba8fb6e0!2sIThis%20-%20State%20Institute%20St.%20Tom%C3%A1s%20de%20Aquino!5e0!3m2!1sen!2sbr!4v1582731533582!5m2!1sen!2sbr"
              width="600"
              height="450"
              frameBorder="0"
              style={{
                border: 0,
                borderRadius: 4,
                margin: 12,
              }}
            />
          </div>
          <div></div>
        </Container>
      </MapContainer>
      <IconHub>
        <IconBase>
          <HistoryIcon />
          <h3 style={{ color: "#fff" }}>História</h3>
          <p style={{ color: "#fff" }}>
            O Instituto Estadual Santo Tomás de Aquino foi fundado em 1957, por
            via de um decreto estadual, e teve diversos nomes como Ginásio Pio
            X, Ginásio Estadual Santo Tomás de Aquino, entre outros; isso por
            conta de suas melhorias administrativas ao decorrer de sua história.
          </p>
        </IconBase>
        <IconBase>
          <CriatividadeIcon />
          <h3 style={{ color: "#fff" }}>Crie</h3>
          <p style={{ color: "#fff" }}>
            Criar coisas é a inovação. O Instituto Estadual Santo Tomás de
            Aquino, por meio de seus projetos como o Núcleo de Ciência e
            Tecnologia, que é voltado à tecnlogia para os jovens, com cursos e
            competições, promove a criatividade como uma forma de conectar
            conhecimentos, vivenciar novas experiências, desenvolver para a
            comunidade, e inovar para o mundo.
          </p>
        </IconBase>
        <IconBase>
          <SocialIcon />
          <h3 style={{ color: "#fff" }}>Página do Facebook</h3>
          <p style={{ color: "#fff" }}>
            Fique por dentro das novidades que estão acontecendo no IESTA, entre
            em contato com a direção, o Grêmio Estudantil e a coordenação, assim
            saiba das notícias através da nossa Página do Facebook, clique{" "}
            <ToFacebookLink
              href="https://www.facebook.com/iesta.marau"
              target="__blank"
            >
              <b>aqui</b>
            </ToFacebookLink>{" "}
            e confira.
          </p>
        </IconBase>
      </IconHub>
      <Footer />
    </>
  );
}
