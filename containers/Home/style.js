import styled from "styled-components";

export const IconHub = styled.div`
  display: flex;
  justify-content: space-evenly;
  background: #ccc;
  margin: 0 auto;
  padding: 24px;
`;

export const IconBaseButton = styled.a`
  background: #f5f5f5;
  color: #000050;
  border: 0 solid #fff;
  border-radius: 12px;
  text-align: center;
  padding: 12px;
  text-decoration: none;
`;

export const IconBase = styled.div`
  text-align: center;
  max-width: 354px;
  min-height: 414px;
  background: #000060;
  padding: 24px 12px;
  border: 0 solid #fff;
  border-radius: 12px;
`;

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const GoodPhrase = styled.h3`
  font-weight: normal;
`;

export const ToFacebookLink = styled.a`
  text-decoration: none;
  color: #3b5998;
  &:hover {
    color: #000090;
  }
`;

export const IconCentre = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 24px;
`;

export const MapContainer = styled.div`
  background-image: linear-gradient(to bottom, #ffffff 0%, #cccccc 100%);
  margin: 0 auto;
  padding: 120px 0;
`;

export const GoodMessage = styled.div`
  font-size: 30px;
  color: #f5f5f5;
  margin: 12px 24px;

  &:hover {
    cursor: default;
  }
`;

export const Background = styled.div`
  background-image: linear-gradient(to left, #6a11cb 0%, #2575fc 100%);
  height: 84vh;
`;

export const Content = styled.div`
  margin: 0 auto;
`;
