import styled from "styled-components";

import { PiOutline } from "styled-icons/typicons/PiOutline";
import { Award } from "styled-icons/boxicons-regular/Award";
import { Project } from "styled-icons/octicons/Project";
import { GitBranch } from "styled-icons/boxicons-regular/GitBranch";

// GoogleMaps
import { Map as MapFindUs } from "styled-icons/feather/Map";

// IconHub
import { BookBookmark } from "styled-icons/foundation/BookBookmark";
import { Facebook } from "styled-icons/entypo-social/Facebook";
import { CubeAlt } from "styled-icons/boxicons-regular/CubeAlt";

export const CriatividadeIcon = styled(CubeAlt)`
  color: #fff;
  border: 0 solid #eee;
  border-radius: 50%;
  width: 30px;
  height: 30px;
  padding: 16px;
  margin-top: 24px;
`;

export const SocialIcon = styled(Facebook)`
  color: #fff;
  border: 0 solid #eee;
  border-radius: 50%;
  width: 30px;
  height: 30px;
  padding: 16px;
  margin-top: 24px;
`;

export const HistoryIcon = styled(BookBookmark)`
  /* color: #4f3961; */
  color: #fff;
  /* background: #eee; */
  border: 0 solid #eee;
  border-radius: 50%;
  width: 30px;
  height: 30px;
  padding: 16px;
  margin-top: 24px;
`;

export const MapIcon = styled(MapFindUs)`
  color: #000050;
  width: 36px;
  height: 36px;
  margin: 12px;
`;

export const CetIcon = styled(PiOutline)`
  color: #000030;
  width: 36px;
  height: 36px;
  margin: 12px;

  &:hover {
    color: #bf0030;
    cursor: pointer;
  }
`;

export const GestaIcon = styled(Award)`
  color: #000030;
  width: 36px;
  height: 36px;
  margin: 12px;

  &:hover {
    color: #7515c5;
    cursor: pointer;
  }
`;

export const InovacaoIcon = styled(Project)`
  color: #000030;
  width: 36px;
  height: 36px;
  margin: 12px;

  &:hover {
    color: #89206e;
    cursor: pointer;
  }
`;

export const GitIcon = styled(GitBranch)`
  color: #000030;
  width: 36px;
  height: 36px;
  margin: 12px;

  &:hover {
    color: #dd5c00;
    cursor: pointer;
  }
`;
