import React from "react";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Global from "../../components/Global";
import { Container, Team, TeacherIcon, Profile } from "./style";
import fetch from "isomorphic-unfetch";
import config from "../../server/config.json";

const EquipeScreen = ({ professores = [] }) => {
  console.log("equipe-screen");
  console.log("equipe-screen props:", professores);
  return (
    <>
      <Global />
      <Header />
      <Container>
        <Team>
          {professores.map((item) => (
            <Profile>
              <div>
                <TeacherIcon />
                <h3>{item.nome_completo}</h3>
              </div>
              <div>
                <h3>Matérias: </h3>
                {item.materias}
              </div>
            </Profile>
          ))}
        </Team>
      </Container>
      <Footer />
    </>
  );
};

EquipeScreen.getInitialProps = async function (context) {
  const response = await fetch(`http://${config.host}:${config.port}/equipe`, {
    method: "GET",
    timeout: 15 * 1000,
  });
  const data = await response.json();
  console.log(`equipe-screen data fetched count: ${data.length}`);

  if (context.query.cargo) {
    return {
      professores: data.professores.filter((item) =>
        item.cargo_id.startsWith(context.query.cargo.toLowerCase())
      ),
    };
  } else if (context.query.sort) {
    return {
      professores: data.sort(),
    };
  }
  return { professores: data.professores };
};

export default EquipeScreen;
