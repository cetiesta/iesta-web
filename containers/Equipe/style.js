import styled from "styled-components";
import { ChalkboardTeacher as Teacher } from "styled-icons/fa-solid/ChalkboardTeacher";

export const TeacherIcon = styled(Teacher)`
  width: 36px;
  height: 36px;
  color: #000070;
`;

export const Profile = styled.div`
  padding: 18px;
  background: #eeeeee;
  min-height: 12vh;
  border: 0 solid #fff;
  border-radius: 8px;
  margin: 12px;
  display: flex;
  flex-direction: column;
`;

export const Container = styled.main`
  background: #f5f5f5;
  display: flex;
  justify-content: center;
`;

export const Team = styled.div`
  margin: 0 auto;
`;
