import styled from "styled-components";

export const Container = styled.main`
  display: flex;
  justify-content: space-around;
  background: #eee;
  padding: 32px;
`;

export const GestaName = styled.h1`
  color: #7515c5;
  background: inherit;
  font-size: 64px;
  text-align: center;
  min-height: 10vh;
  font-weight: bold;
  cursor: default;
`;

export const ExecutiveDepartmentPaperContainer = styled.div`
  background: #7515c5;
  min-width: 98.9vw;
  height: 100vh;
  padding: 8px 0;
`;

export const Title = styled.h3`
  color: #fff;
  font-size: 64px;
  font-family: "Open Sans", "Roboto", sans-serif;
  margin-bottom: 12px;
`;

export const ExpandedPart = styled.div`
  max-width: 50%;
`;

export const ExpandedContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 24px;
`;

export const FlexContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 6px;
`;
