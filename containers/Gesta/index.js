import React from "react";
import { DownloadButton, Footer, Global, Header } from "../../components";
import * as styled from "./style";
import { Card, CardContent, Typography } from "@material-ui/core";
import AssistantPhotoIcon from "@material-ui/icons/AssistantPhoto";
import EmojiPeopleIcon from "@material-ui/icons/EmojiPeople";
import SchoolIcon from "@material-ui/icons/School";
import DescriptionIcon from "@material-ui/icons/Description";

export default function GestaScreen() {
  return (
    <>
      <Global />
      <Header />
      <styled.Container>
        <Card container xs={12} style={{ cursor: `default` }}>
          <styled.Container>
            <Card
              item
              xs={12}
              variant="outlined"
              style={{ maxWidth: 310, height: 283 }}
            >
              <CardContent>
                <styled.FlexContainer>
                  <AssistantPhotoIcon
                    fontSize="large"
                    style={{ color: "#7515c5", margin: 6 }}
                  />
                  <Typography variant="h5">Direção Escolar</Typography>
                </styled.FlexContainer>
                <styled.FlexContainer>
                  <Typography
                    variant="p"
                    style={{ fontWeight: "normal", textAlign: "justify" }}
                  >
                    A Direção é totalmente ligada ao Grêmio Estudantil,
                    participando das decisões, do planejamento interno e dos
                    eventos extracurriculares gerenciados pelo Grêmio
                    Estudantil. O <strong>Conselho de Professores</strong>,
                    junto à <strong>Direção Escolar</strong>, é quem escolhe os
                    estudantes que irão servir aos cargos do Grêmio Estudantil.
                  </Typography>
                </styled.FlexContainer>
              </CardContent>
            </Card>
            <Card
              item
              xs={12}
              variant="outlined"
              style={{ maxWidth: 310, height: 283 }}
            >
              <CardContent>
                <styled.FlexContainer>
                  <SchoolIcon
                    fontSize="large"
                    style={{ color: "#7515c5", margin: 6 }}
                  />
                  <Typography variant="h5">Executivo</Typography>
                </styled.FlexContainer>
                <styled.FlexContainer>
                  <Typography
                    variant="p"
                    style={{ fontWeight: "normal", textAlign: "justify" }}
                  >
                    O Departamento Executivo do GESTA é responsável por manter
                    os ideias da Direção dentro entre os avanços do Grêmio,
                    sendo chefiado pelo(a){" "}
                    <strong>Presidente do Grêmio Estudantil</strong>, que possui
                    um mandato de 1 Ano. Seu gabinete é composto pelo o(a){" "}
                    <strong>Vice-Presidente</strong> e o(a) Secretário Geral e
                    os <strong>Secretários</strong> de específicas áreas.
                  </Typography>
                </styled.FlexContainer>
              </CardContent>
            </Card>
            <Card
              item
              xs={12}
              variant="outlined"
              style={{ maxWidth: 310, height: 283 }}
            >
              <CardContent>
                <styled.FlexContainer>
                  <EmojiPeopleIcon
                    fontSize="large"
                    style={{ color: "#7515c5", margin: 6 }}
                  />
                  <Typography variant="h5">Asssembleia</Typography>
                </styled.FlexContainer>
                <styled.FlexContainer>
                  <Typography
                    variant="p"
                    style={{ fontWeight: "normal", textAlign: "justify" }}
                  >
                    A <strong>Assembleia dos Representantes</strong> é o
                    Departamento Legislativo do Grêmio Estudantil, que age por
                    meio de projetos para criar e legislar sobre as questões de
                    responsabilidade do Grêmio Estudantil. É composta por Um
                    Aluno de cada Turma do IESTA deste as Turmas do{" "}
                    <strong>8ᵒ Ano do Ensino Fundamental</strong> até as Turmas
                    do <strong>3ᵒ Ano do Ensino Médio</strong>.
                  </Typography>
                </styled.FlexContainer>
              </CardContent>
            </Card>
          </styled.Container>
        </Card>
      </styled.Container>
      <styled.Container>
        <styled.GestaName>
          Grêmio Estudantil Santo Tomás de Aquino
        </styled.GestaName>
      </styled.Container>
      <styled.Container>
        <styled.FlexContainer>
          <styled.ExecutiveDepartmentPaperContainer>
            <styled.ExpandedContainer>
              <styled.ExpandedPart>
                <styled.FlexContainer style={{ alignItems: "center" }}>
                  <DescriptionIcon style={{ marginRight: 8, color: "#fff" }} />
                  <styled.Title>Estatuto</styled.Title>
                </styled.FlexContainer>
                <Typography variant="h6" style={{ color: "#fff" }}>
                  O nosso Estatuto Social do Grêmio Estudantil Santo Tomás de
                  Aquino está em PDF caso você queira vê-lo!
                </Typography>
                <DownloadButton
                  title="Baixar o Estatuto Social do GESTA"
                  file="estatuto-gesta"
                  background="#fff"
                  color="#7515c5"
                />
                <br />
                <br />
                <br />
                <br />
                <br />
                <div>
                  <br />
                  <Typography
                    variant="p"
                    style={{ color: "#eee", textAlign: "justify" }}
                  >
                    <i>
                      "
                      <strong>
                        O Grêmio Estudantil é a representação dos estudantes na
                        administração da escola!
                      </strong>
                      <br />
                      <span>
                        Toda Turma do Instituto possui, no mínimo, um
                        representante no Grêmio Estudantil, o que é obrigatório.
                      </span>
                    </i>
                  </Typography>
                </div>
              </styled.ExpandedPart>
              <styled.ExpandedPart>
                <EmojiPeopleIcon
                  style={{
                    color: "#fff",
                    height: `60vh`,
                    width: `60%`,
                    position: "relative",
                  }}
                />
              </styled.ExpandedPart>
            </styled.ExpandedContainer>
          </styled.ExecutiveDepartmentPaperContainer>
        </styled.FlexContainer>
      </styled.Container>
      <Footer />
    </>
  );
}
