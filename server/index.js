// const express = require("express");
// const router = require("./router");
const database = require("./database");
// const config = require("./config.json");

module.exports = function run() {
  database.run();
};

// const application = express();

// application.use(express.json());
// application.use(express.urlencoded({ extended: true }));
// application.use(router);

// application.listen(parseInt(config.port), () => {
//   console.log(`[IESTA] Back-end ouvindo à porta ${config.port}`);
//   database.run();
// });
