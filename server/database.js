const { Model, Sequelize, DataTypes } = require("sequelize");
const configuration = require("./config.json");

class Cargo extends Model {}

const initCargo = (sequelize) => {
  Cargo.init(
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
        allowNull: false,
      },
      color: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "#fff",
      },
      permission_level: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
    },
    { sequelize, modelName: "cargo" }
  );
  console.log("[IESTA] initCargo().");
};

class Professor extends Model {}

const initProfessor = (sequelize) => {
  Professor.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      nome_completo: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      aniversario_data: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      materias: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      cargo_id: {
        type: DataTypes.ENUM,
        values: [
          "estagiario",
          "professor",
          "coordenador",
          "vice_diretor",
          "diretor",
        ],
      },
    },
    { sequelize, modelName: "professor" }
  );
  console.log("[IESTA] initProfessor().");
};

module.exports = {
  run: () => {
    const sequelize = new Sequelize(configuration);
    initCargo(sequelize);
    initProfessor(sequelize);
    sequelize.sync().then(() => {
      console.log("[IESTA] Tables sincronizadas.");
    });
  },
  Cargo: Cargo,
  Professor: Professor,
};
