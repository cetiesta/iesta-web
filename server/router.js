const express = require("express");
const Router = express.Router();
const database = require("./database");

Router.use(function timeLog(request, response, next) {
  console.log("Time: ", Date.now());
  next();
});

Router.get("/equipe", (request, response) => {
  database.Professor.findAll()
    .then(result => {
      return response.json({ professores: result });
    })
    .catch(error => error);
});

Router.get("/equipe/:id", (request, response) => {
  const { id } = request.params;
  console.log(`procurando id ${id}...`);
  if (!id) {
    console.log(`nada com id ${id}!`);
    return null;
  }
  database.Professor.findByPk(id)
    .then(result => {
      return response.json(result);
    })
    .catch(error => error);
});

module.exports = Router;
