import fetch from "isomorphic-unfetch";

const viewOrDownloadPDF = (file) => {
  file = file.replace(".pdf", "");
  fetch(`static/${file}.pdf`).then((response) => {
    response.blob().then((blob) => {
      let url = window.URL.createObjectURL(blob);
      let a = document.createElement("a");
      a.target = "_blank";
      a.href = url;
      a.download = `${file}.pdf`;
      // a.click();

      try {
        window.location.href = response.url;
        console.log(
          "DownloadButton => enviar visitante ao PDF locatizado em:",
          response.url
        );
      } catch (error) {
        console.log(
          "DownloadButton => Como o navegador utilizado não possui support-PDF typefile, download retornado."
        );
        a.click();
      }
    });
    //   window.location.href = response.url;
  });
};

export default viewOrDownloadPDF;
