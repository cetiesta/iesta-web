import React from "react";
import { Button } from "@material-ui/core";
import GetAppIcon from "@material-ui/icons/GetApp";
import viewOrDownloadPDF from "./viewOrDownloadPDF";

export default function DownloadButton(props) {
  const { color } = props;
  return (
    <Button
      style={{
        background: props.background ? props.background : "#fff",
        marginTop: 12,
        float: "right",
      }}
      variant="contained"
      startIcon={<GetAppIcon style={{ color }} />}
      onClick={viewOrDownloadPDF(props.file)}
    >
      BAIXAR PDF
    </Button>
  );
}
