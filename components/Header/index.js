import React from "react";
import * as styled from "./style";
import * as icons from "./icons";

export default function Header() {
  return (
    <styled.Container>
      <div>
        <a href="/">
          <styled.Logo
            src="/static/icon.png"
            alt="Logo do Instituto Estadual Santo Tomás de Aquino"
            title="Instituto Estadual Santo Tomás de Aquino"
          />
        </a>
      </div>
      <div></div>
      <styled.LinkGroup>
        <styled.Link href="/">
          <icons.HomeIcon />
          <div>Início</div>
        </styled.Link>
        <styled.Link href="/equipe">
          <icons.TeamIcon />
          <div>Professores</div>
        </styled.Link>
        <styled.Link href="/gesta">
          <icons.GestaIcon />
          <div>Grêmio Estudantil</div>
        </styled.Link>
        <styled.Link href="/cet">
          <icons.CetIcon />
          <div>Ciência e Tecnologia</div>
        </styled.Link>
        <styled.Link href="/biblioteca">
          <icons.BibliotecaIcon />
          <div>Biblioteca</div>
        </styled.Link>
      </styled.LinkGroup>
    </styled.Container>
  );
}
