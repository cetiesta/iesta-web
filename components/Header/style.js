import styled from "styled-components";

export const Container = styled.nav`
  background: #fff;
  color: #000;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const LinkGroup = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  margin: 0 16px;
`;

export const Logo = styled.img`
  width: 74px;
  height: 84px;
  padding: 8px;
`;

export const Link = styled.a`
  background: #eee;
  border: 0 solid #eee;
  border-radius: 6px;
  font-size: 18px;
  display: flex;
  align-items: center;
  text-decoration: none;
  justify-content: space-evenly;
  margin: 4px 8px;
  padding: 8px;
  color: #000;

  &:hover {
    cursor: pointer;
    background: #ddd;
  }
`;
