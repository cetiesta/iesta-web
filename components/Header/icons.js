import styled from "styled-components";

import { Home } from "styled-icons/heroicons-outline/Home";
import { Team } from "styled-icons/remix-line/Team";
import { PeopleCarry } from "styled-icons/fa-solid/PeopleCarry";
import { Robot } from "styled-icons/fa-solid/Robot";
import Book from "@material-ui/icons/Book";

export const BibliotecaIcon = styled(Book)`
  width: 16px;
  height: 16px;
  margin: 2px;
  margin-right: 10px;
`;

export const CetIcon = styled(Robot)`
  color: #000;
  width: 16px;
  height: 16px;
  margin: 2px;
  margin-right: 10px;
`;

export const GestaIcon = styled(PeopleCarry)`
  color: #000;
  width: 16px;
  height: 16px;
  margin: 2px;
  margin-right: 10px;
`;

export const HomeIcon = styled(Home)`
  color: #000;
  width: 16px;
  height: 16px;
  margin: 2px;
  margin-right: 10px;
`;

export const TeamIcon = styled(Team)`
  color: #000;
  width: 16px;
  height: 16px;
  margin: 2px;
  margin-right: 10px;
`;
