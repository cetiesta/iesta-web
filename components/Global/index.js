import { createGlobalStyle } from "styled-components";

const Global = createGlobalStyle`
    body {
        font-family: "Roboto", "Open Sans", Arial, sans-serif;
        padding: 0;
        margin: 0;
    }
`;

export default Global;
