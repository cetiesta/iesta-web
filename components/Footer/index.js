import React from "react";
import { Container } from "./style";

export default function Footer() {
  return (
    <Container>
      <div></div>
      <div>
        Instituto Estadual Santo Tomás de Aquino &copy;{" "}
        {new Date().getFullYear()}
      </div>
      <div></div>
    </Container>
  );
}
