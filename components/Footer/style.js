import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  justify-content: space-evenly;
  background: #000050;
  padding: 12px;
  color: #fff;
`;
